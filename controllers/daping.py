# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json
import jinja2
import logging
from odoo import http
import json
import random
from odoo.http import request
from pyecharts.charts import Bar
from pyecharts import options as opts
from pyecharts.charts import Line
from pyecharts.charts import Pie
from pyecharts.charts import Gauge
from pyecharts.charts import HeatMap
from pyecharts.faker import Faker

from pyecharts.components import Image
from pyecharts.options import ComponentTitleOpts

from pyecharts.commons.utils import JsCode

from odoo import http

_logger = logging.getLogger(__name__)

# ----------------------------------------------------------
# Controllers
# ----------------------------------------------------------

loader = jinja2.PackageLoader('odoo.addons.library_pyecharts', "templates")

jinja_env = jinja2.Environment(loader=loader, autoescape=True)
jinja_env.filters["json"] = json.dumps

homepage_template = jinja_env.get_template('homepage.html')


class HtmlController(http.Controller):

    @http.route('/homepage', type='http', auth='none')
    def index(self):
        return jinja_env.get_template('homepage.html').render()

    @http.route('/index', type='http', auth='none')
    def index(self):
        return jinja_env.get_template('index.html').render()

    @http.route('/get_daping_pie', auth='public', type='http', cors='*', methods=['POST', 'GET'], csrf=False)
    def demo(self):
        return json.dumps({
            'pie': json.loads(daping_pie_base()),
            'bar': json.loads(daping_bar_base()),
            'line': json.loads(daping_line_base()),
        })


def daping_pie_base():
    c = (
        Pie()
            .add("", [list(z) for z in zip(Faker.choose(), Faker.values())], radius=['30%', '50%'])
            .set_series_opts(label_opts=opts.LabelOpts(formatter="{b}: {c}"))
            .set_global_opts(
            toolbox_opts=opts.ToolboxOpts(is_show=False),
            legend_opts=opts.LegendOpts(is_show=True),
        )
            .dump_options_with_quotes()
    )
    return c


def daping_bar_base():
    c = (
        Bar()
            .add_xaxis(["衬衫", "羊毛衫", "雪纺衫"])
            .add_yaxis("商家A", [100, 200, 300], bar_width='100%')
            .add_yaxis("商家B", [123, 334, 333], bar_width='100%')
            .dump_options_with_quotes()
    )
    return c


def daping_line_base():
    x_data = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    y_data = [820, 932, 901, 934, 1290, 1330, 1320]

    c = (
        Line()
            .set_global_opts(
            tooltip_opts=opts.TooltipOpts(is_show=False),
            toolbox_opts=opts.ToolboxOpts(is_show=False, pos_left='90%', pos_top='top'),
            xaxis_opts=opts.AxisOpts(type_="category"),
            yaxis_opts=opts.AxisOpts(
                type_="value",
                axistick_opts=opts.AxisTickOpts(is_show=True),
                splitline_opts=opts.SplitLineOpts(is_show=True),
            ),
        )
            .add_xaxis(xaxis_data=x_data)
            .add_yaxis(
            series_name="",
            y_axis=y_data,
            symbol="emptyCircle",
            is_symbol_show=True,
            label_opts=opts.LabelOpts(is_show=True),
            markline_opts=opts.MarkLineOpts(data=[opts.MarkLineItem(type_="average")]),
        )
            .dump_options_with_quotes()
    )
    return c

